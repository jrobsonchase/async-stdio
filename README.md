# Adapter for using async read/write streams in std::io contexts

[![version](https://img.shields.io/crates/v/async-stdio.svg)](https://crates.io/crates/async-stdio/)
[![documentation](https://docs.rs/async-stdio/badge.svg)](https://docs.rs/async-stdio/)
[![license](https://img.shields.io/crates/l/async-stdio.svg)](https://crates.io/crates/async-stdio/)

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or <http://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
